<?php


namespace App\Http\Traits;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

trait DataModels
{


    public function fetchStates($select = '*'): Collection
    {
        $states = DB::table('states')->select($select);

        return $states->orderBy('state_name')->get();
    }


    public function fetchLgas($select = '*', $where = [])
    {
        $lgas = DB::table('lga')->select($select);

        $where ? $lgas = $lgas->where(...$where) : "";

        return $lgas->orderBy('lga_name')->get();
    }


    public function fetchWards($select = '*', $where = [])
    {
        $wards = DB::table('ward')->distinct()->select($select);

        $where ? $wards = $wards->where(...$where) : "";

        return $wards->orderBy('ward_name')->get();
    }


    public function fetchParties($where = [])
    {
        $parties = DB::table('party')->distinct();

        $where ? $parties = $parties->where(...$where) : '';

        return $parties->orderBy('partyname')->get();
    }
}
