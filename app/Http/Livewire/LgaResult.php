<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\DB;
use Livewire\Component;

class LgaResult extends Component
{

    public $lgas, $lga, $lga_name;
    public $result;

    protected $listeners = [
        'checkResult'
    ];


    public function checkResult($lga_name)
    {
        $this->reset('result');
        $this->lga_name = $lga_name;
        if (!empty($this->lga))
        {
            $this->result = (array) DB::table('announced_pu_results')
                ->join('polling_unit', 'announced_pu_results.polling_unit_uniqueid', '=', 'polling_unit.uniqueid')
                ->join('lga', 'polling_unit.lga_id', '=', 'lga.lga_id')
                ->select(DB::raw('SUM(announced_pu_results.party_score) as party_score'))
                ->where('lga.lga_id', $this->lga)
                ->first();
        }
    }


    public function render()
    {

        $this->lgas = DB::table('lga')->get();
        return view('lga-result')->layoutData([
            'page_title' => 'LGA Results'
        ]);
    }
}
