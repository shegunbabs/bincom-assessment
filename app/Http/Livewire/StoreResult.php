<?php

namespace App\Http\Livewire;

use App\Http\Traits\DataModels;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class StoreResult extends Component
{

    public $states;
    public $lgas, $lga="";
    public $wards, $ward = "";
    public $pollingUnits, $pollingUnit;
    public $parties;
    public $showNewPuResultForm = false;
    public $party = [];
    /**
     * @var mixed|void
     */
    public $pollingUnitData;

    use DataModels;


    public function submitResult()
    {
        $validated = $this->validate($this->validationFields(), [], $this->validationFieldNames());
        //dd($validated);

        //insert result here
        $inserts = [];
        foreach ($validated['party'] as $key => $value)
        {
            $inserts[] = [
                'polling_unit_uniqueid' => $this->pollingUnit,
                'party_abbreviation' => $key,
                'party_score' => $value,
                'entered_by_user' => 'segun babs',
                'date_entered' => now(),
                'user_ip_address' => request()->ip()
            ];
        }
        //dd($inserts);
        DB::table('announced_pu_results')->insert($inserts);

        flash()->overlay("Results have been saved successfully", "Success")->livewire($this);
        $this->reset('party');
        $this->getPollingUnits($this->ward);
    }


    public function getParties()
    {
        $this->parties = $this->fetchParties();
    }


    public function checkForPuResult($pu_uniqueid): void
    {
        $this->resetErrorBag();
        if ( !empty($pu_uniqueid) ){
            $puResult = DB::table('announced_pu_results')->where('polling_unit_uniqueid', $pu_uniqueid)->first();

            if ( !empty($puResult)  ){
                $this->puHasResults();
                return;
            }
            $this->puHasNoResult();
            return;
        }
        $this->showNewPuResultForm = false;
    }


    public function puHasResults()
    {
        $this->showNewPuResultForm = false;
        flash()->overlay("This polling unit already has results", "Exisiting Results")->livewire($this);
    }


    public function puHasNoResult()
    {
        $this->showNewPuResultForm = true;
        $this->getParties();
        $this->pollingUnitData = (array) DB::table('polling_unit')->where('uniqueid', $this->pollingUnit)->first();
    }


    public function getPollingUnits($ward_id)
    {
        $this->showNewPuResultForm = false;
        $this->reset('pollingUnits');
        $this->pollingUnit = "";

        if ( $ward_id )
        {
            $this->pollingUnits = DB::table('polling_unit')
                ->distinct()
                ->where('ward_id', (int) $ward_id)
                ->where('lga_id', (int) $this->lga)
                ->get();
        }
    }


    public function getWards($lga_id)
    {
        //$lga is set here
        $this->ward = "";
        $this->reset('wards');
        if ( $lga_id )
        {
            $this->wards = $this->fetchWards(where: ['lga_id', $lga_id]);
        }
    }


    public function getLgas($state_id)
    {
        $this->lga = ""; $this->ward = "";
        $this->reset('lgas');
        if ($state_id)
        {
            $this->lgas = $this->fetchLgas('*', ['state_id', '=', $state_id]);
        }
    }


    public function render()
    {

        $this->states = $this->fetchStates();

        return view('store-result')
            ->layoutData([
                'page_title' => 'Insert Result'
            ]);
    }

    private function validationFieldNames()
    {
        $out = [];
        foreach ($this->parties as $party)
        {
            $out["party.".$party['partyname']] = $party['partyname'] . " score";
        }
        return $out;
    }

    private function validationFields()
    {
        $out = [];
        foreach ($this->parties as $party)
        {
            $out["party.".$party['partyname']] = 'required|numeric';
        }
        return $out;
    }
}
