<?php

namespace App\Http\Livewire;

use App\Http\Traits\DataModels;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class PollingUnitResult extends Component
{
    public $states;
    public $lgas;
    public $wards;
    public $pus;

    public $state;
    public $state_name;
    public $lga, $lga_name;
    public $ward, $ward_name;
    public $pu, $pu_data;
    public $results;

    protected $listeners = [
        'fetchLgas', 'fetchWards', 'fetchPus', 'fetchResults'
    ];

    use DataModels;


    public function fetchLgas($state): void
    {
        $this->reset(['lga', 'lga_name', 'wards', 'ward', 'pus', 'ward_name', 'pu', 'pu_data', 'results']);
        $this->lgas = "";
        if (!empty($this->state))
        {
            $this->state_name = (DB::table('states')->select('state_name')->where('state_id', $state)->first())->state_name;
            $this->lgas = DB::table('lga')->where('state_id', (int) $state)->get();
        }
    }


    public function fetchWards($lga): void
    {
        $this->ward = "";
        $this->reset(['pus', 'ward_name', 'pu', 'pu_data', 'results', 'wards']);
        if ( $lga )
        {
            $this->lga_name = (DB::table('lga')->select('lga_name')->where('lga_id', $lga)->first())->lga_name;
            $this->wards = DB::table('ward')->distinct()->where('lga_id', $lga)->orderBy('ward_name')->get();
        }
    }


    public function fetchPus($ward): void
    {
        $this->reset(['pu', 'pu_data', 'results', 'pus']);
        $this->pu = "";
        if ( $ward )
        {
            $this->ward_name = (DB::table('ward')
                ->select('ward_name')
                ->where('ward_id', $ward)
                ->where('lga_id', $this->lga)
                ->first())->ward_name;
            $this->pus = DB::table('polling_unit')
                ->distinct()
                ->where('ward_id', (int) $ward)
                ->where('lga_id', (int) $this->lga)
                ->get();
        }
    }


    public function fetchResults($pu)
    {
        $this->reset('results');
        $this->pu_data = DB::table('polling_unit')->where('uniqueid', $pu)->get();
        //dd($this->pu_data);
        if ( empty($pu) ){
            $this->results = "";
            return;
        }
        $this->results = DB::table('announced_pu_results')
            ->where('polling_unit_uniqueid', $pu)
            ->orderBy('party_abbreviation')
            ->get();
    }


    public function render()
    {
        $this->states = $this->fetchStates();

        return view('polling-unit-result')->layoutData(['page_title' => "Polling Unit Result"]);
    }
}
