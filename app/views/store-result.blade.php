<div class="max-w-screen-md">
    {{-- Stop trying to control. --}}
    <form class="space-y-8 divide-y divide-gray-200">
        <div class="space-y-8 divide-y divide-gray-200">
            <div class="pt-8">
                <div>
                    <h3 class="text-lg leading-6 font-medium text-gray-900">
                        Polling Unit Results
                    </h3>
                    <p class="mt-1 text-sm text-gray-500">
                        Choose a polling unit to store it's result
                    </p>
                </div>
                <div class="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">

                    <div class="sm:col-span-2">
                        <x-form.label for="state">State</x-form.label>
                        <div class="mt-1">
                            <x-form.select id="state" wire:change="getLgas($event.target.value)">
                                <option value="">Select One</option>
                                @foreach($states as $_state)
                                    <x-form.option :value="$_state->state_id">{{ $_state->state_name }}</x-form.option>
                                @endforeach
                            </x-form.select>
                        </div>
                    </div>

                    <div class="sm:col-span-2">
                        <x-form.label for="lga">Local Govt. Area (LGA): {{ !empty($lgas) ? count($lgas) : 0 }}</x-form.label>
                        <div class="mt-1">
                            <x-form.select wire:model="lga" wire:change="getWards($event.target.value)">
                                <option value="">Select One</option>
                                @if ($lgas)
                                    @foreach($lgas as $_lga)
                                        <x-form.option
                                            :value="$_lga->lga_id ?? $_lga['lga_id']">{{ $_lga->lga_name ?? $_lga['lga_name'] }}</x-form.option>
                                    @endforeach
                                @endif
                            </x-form.select>
                        </div>
                    </div>

                    <div class="sm:col-span-2">
                        <x-form.label>Ward: {{ !empty($wards) ? count($wards) : 0 }}</x-form.label>
                        <div class="mt-1">
                            <x-form.select wire:model="ward" wire:change="getPollingUnits($event.target.value)">
                                <option value="">Select One</option>
                                @if ( $wards )
                                    @foreach($wards as $_ward)
                                        <x-form.option
                                            :value="$_ward->ward_id ?? $_ward['ward_id']">{{ $_ward->ward_name ?? $_ward['ward_name'] }}</x-form.option>
                                    @endforeach
                                @endif
                            </x-form.select>
                        </div>
                    </div>

                    <div class="sm:col-span-6 border-b"></div>

                    <div class="sm:col-span-3">
                        <x-form.label>Polling
                            Unit: {{ !empty($pollingUnits) ? count($pollingUnits) : 0 }}</x-form.label>
                        <div class="mt-1">
                            <x-form.select wire:model="pollingUnit" wire:change="checkForPuResult($event.target.value)">
                                <option value="">Select One</option>
                                @if ( $pollingUnits )
                                    @foreach($pollingUnits as $pu)
                                        <x-form.option
                                            :value="$pu->uniqueid ?? $pu['uniqueid']">{{ $pu->polling_unit_name ?? $pu['polling_unit_name'] }}</x-form.option>
                                    @endforeach
                                @endif
                            </x-form.select>
                        </div>
                    </div>


                    @if ( $showNewPuResultForm === true )
                        <div class="sm:col-span-6 mt-6 sm:mt-5 space-y-6 sm:space-y-5 relative">
                            <div class="text-xl text-blue-700 font-semibold">Polling Unit Number: {{ $pollingUnitData['polling_unit_number'] }}</div>
                                @if($showNewPuResultForm && $parties)
                                    @foreach($parties as $_party)
                                        <div class="sm:grid sm:grid-cols-3 sm:items-start sm:border-t sm:border-gray-200 sm:pt-3">
                                            <x-form.label class="text-right px-4 place-self-center">{{ $_party->partyname ?? $_party['partyname'] }}</x-form.label>
                                            <div class="mt-1 sm:mt-0 sm:col-span-2">
                                                <x-form.input wire:model="party.{{ $_party->partyname ?? $_party['partyname'] }}" type="number" placeholder="{{ $_party->partyname ?? $_party['partyname'] }} score"/>
                                            </div>
                                            <x-form.input-error for="party.{{ $_party->partyname ?? $_party['partyname'] }}" class="col-start-2" />
                                        </div>
                                    @endforeach
                                @endif
                            <x-livewire-loading wire:target="submitResult" class="opacity-50" />
                        </div>
                    @endif


                </div>
            </div>

            @if ( $showNewPuResultForm === true )
                <div class="pt-5">
                    <div class="flex justify-end">
                        <button type="button"
                                class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Cancel
                        </button>
                        <button wire:click.prevent="submitResult" type="button"
                                class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Save
                        </button>
                    </div>
                </div>
            @endif
        </div>
    </form>

</div>
