<div>
    {{-- The best athlete wants his opponent at his best. --}}
    <form class="space-y-8 divide-y divide-gray-200">
        <div class="space-y-8 divide-y divide-gray-200">
            <div>
                <div>
                    <h3 class="text-lg leading-6 font-medium text-gray-900">
                        Results by Polling Unit
                    </h3>
                    <p class="mt-1 text-sm text-gray-500">
                        Choose a State, LGA, Ward and Polling unit to see results of the polling unit
                    </p>
                </div>

                <div class="mt-6 grid grid-cols-1 gap-y-6 gap-x-10 sm:grid-cols-4 relative px-1 py-1 mb-4">
                    <div class="sm:col-span-1">
                        <x-form.label for="state">State</x-form.label>
                        <div class="mt-1">
                            <select wire:model="state" wire:change="$emitSelf('fetchLgas', $event.target.value)"
                                    class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md">
                                <option value="">Select One</option>
                                @foreach($states as $_state)
                                    <x-form.option :value="$_state->state_id">{{ $_state->state_name }}</x-form.option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="sm:col-span-1">
                        <x-form.label for="lga">LGA</x-form.label>
                        <div class="mt-1 flex items-center">
                            @if( !empty($lgas) && count($lgas) )
                                <select wire:model="lga" wire:change="$emitSelf('fetchWards', $event.target.value)"
                                        class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md">
                                    <option value="">Select One</option>
                                    @foreach($lgas as $_lga)
                                        <option
                                            value="{{ $_lga->lga_id ?? $_lga['lga_id'] }}">{{ $_lga->lga_name ?? $_lga['lga_name'] }}</option>
                                    @endforeach
                                </select>
                            @else
                                <x-alert type="info">No data for selected state</x-alert>
                            @endif
                        </div>
                    </div>

                    <div class="sm:col-span-1">
                        <x-form.label for="ward">Ward</x-form.label>
                        <div class="mt-1">
                            @if( !empty($wards) && count($wards) )
                                <x-form.select wire:model="ward"
                                               wire:change="$emitSelf('fetchPus', $event.target.value)">
                                    <option value="">Select One</option>
                                    @foreach($wards as $_ward)
                                        <x-form.option
                                            :value="$_ward->ward_id ?? $_ward['ward_id']">{{ $_ward->ward_name ?? $_ward['ward_name'] }}</x-form.option>
                                    @endforeach
                                </x-form.select>
                            @else
                                <x-alert type="info">No Ward for selected lga</x-alert>
                            @endif
                        </div>
                    </div>

                    <div class="sm:col-span-1">
                        <x-form.label for="pus">Polling Units</x-form.label>
                        <div class="mt-1">
                            @if( !empty($pus) && count($pus) )
                                <x-form.select wire:model="pu"
                                               wire:change="$emitSelf('fetchResults', $event.target.value)">
                                    <option value="">Select One</option>
                                    @foreach($pus as $_pu)
                                        <x-form.option
                                            :value="$_pu->uniqueid ?? $_pu['uniqueid']">{{ $_pu->polling_unit_name ?? $_pu['polling_unit_name'] }}</x-form.option>
                                    @endforeach
                                </x-form.select>
                            @else
                                <x-alert type="info">No PUs for selected ward</x-alert>
                            @endif
                        </div>
                    </div>

                    <x-livewire-loading class="px-3 py-1 opacity-50"/>

                </div>

                <div class="border-t-2 grid grid-cols-1 sm:grid-cols-3 pt-6">
                    <div class="sm:col-span-2 transition duration-500 ease-in-out">
                        @if( !empty($results) && !empty($pu) )
                            <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                                <div class="px-4 py-5 sm:px-6">
                                    <h3 class="text-4xl leading-6 font-medium text-gray-900">
                                        {{ ucfirst($state_name) . " State" }} <small
                                            class="text-base">{{ ucfirst($lga_name) . " Local Govt." }}</small>
                                    </h3>
                                    <p class="mt-1 max-w-2xl text-sm text-gray-500">
                                        <div class="my-6">
                                            <span>Ward:</span> <span class="bg-gray-100 rounded px-2 py-1">{{ $ward_name }}</span>
                                        </div>
                                        <div>
                                            <span>POLLING UNIT:</span> <span class="py-1 px-2">{{ $pu_data[0]->polling_unit_name ?? "" }}
                                                ({{ $pu_data[0]->polling_unit_number ?? "" }})</span>
                                        </div>
                                    </p>
                                </div>
                                <div class="border-t border-gray-200 px-4 py-5 sm:p-0">
                                    <dl class="sm:divide-y sm:divide-gray-200">

                                        @if( $results->count() )
                                            @foreach( $results as $result )
                                                <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                                    <dt class="text-sm font-medium text-blue-800">
                                                        {{ $result->party_abbreviation }}
                                                    </dt>
                                                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                                        {{ $result->party_score }}
                                                    </dd>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 whitespace-nowrap text-yellow-800 bg-yellow-50">
                                                There are no election results for {{ $pu_data[0]->polling_unit_name ?? "" }}
                                            </div>
                                        @endif
                                    </dl>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
