<div>
    {{-- Success is as dangerous as failure. --}}
    <div class="space-y-8 divide-y divide-gray-200">
        <div class="relative max-w-md">
            <x-livewire-loading class="opacity-50" />
            <div>
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                    Results by Local Government Areas
                </h3>
                <p class="mt-1 text-sm text-gray-500">
                    Choose a LGA to see results.
                </p>
            </div>

            <div class="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 p-2">
                <div class="sm:col-span-1">
                    <x-form.label for="lgas">LGA</x-form.label>
                    <div class="mt-1">
                        <x-form.select wire:model="lga" wire:change="$emitSelf('checkResult', $event.target.selectedOptions[0].text)" id="lgas">
                            <option value="">Select One</option>
                            @foreach($lgas as $_lga)
                                <x-form.option :value="$_lga->lga_id">{{ $_lga->lga_name }}</x-form.option>
                            @endforeach
                        </x-form.select>
                    </div>
                </div>
            </div>

            @if( !is_null($result) && count($result) )
                <div class="bg-white shadow overflow-hidden sm:rounded-lg w-full my-6">
                    <div class="px-4 py-5 sm:px-6">
                        <h3 class="text-lg leading-6 font-medium text-gray-900">
                            Total for  {{ $lga_name }}
                        </h3>
                        <p class="mt-1 text-2xl font-semibold text-gray-700">
                            {{ number_format($result['party_score']) ?? "0" }}
                        </p>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
