@props([
    "type" => "info",
])
<?php
    $styles = [
        "info" => "bg-yellow-50 border border-yellow-200 text-yellow-800",
        "error" => "bg-red-50 border border-red-200 text-red-800",
        "success" => "bg-green-50 border border-green-200 text-green-800",
    ];
?>
<div {{ $attributes->merge(["class" => "text-xs px-2 py-2 rounded w-full $styles[$type]"]) }}>
    {{ $slot }}
</div>
