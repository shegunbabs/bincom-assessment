@props([
    "value" => null,
    "selected" => "",
])
<option value="{{ $value }}">{{ $slot }}</option>
