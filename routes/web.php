<?php

use App\Http\Livewire\LgaResult;
use App\Http\Livewire\PollingUnitResult;
use App\Http\Livewire\StoreResult;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/polling-unit-results');

Route::get('polling-unit-results', PollingUnitResult::class)->name('pu.results');
Route::get('lga-results', LgaResult::class)->name('lga.results');
Route::get('store-result', StoreResult::class)->name('store.results');
